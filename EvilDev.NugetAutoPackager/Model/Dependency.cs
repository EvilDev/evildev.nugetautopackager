﻿namespace EvilDev.NugetAutoPackager.Model
{
    public class Dependency
    {
        public string Id { get; set; }

        public string Version { get; set; }
    }
}
