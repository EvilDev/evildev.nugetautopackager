﻿using System.Collections.Generic;

namespace EvilDev.NugetAutoPackager.Model
{
    public class Nuspec
    {
        public Nuspec()
        {
            Dependencies = new List<Dependency>();
            Owners = new List<string>();
            FrameworkAssemblies = new List<FrameworkAssembly>();
            Tags = new List<string>();
            Authors = new List<string>();
            Files = new List<File>();
        }

        public string Id { get; set; }

        public string Version { get; set; }

        public string Title { get; set; }

        public ICollection<string> Authors { get; private set; }

        public ICollection<string> Owners { get; private set; }

        public string Description { get; set; }

        public string ReleaseNotes { get; set; }

        public string Summary { get; set; }

        public string Language { get; set; }

        public string ProjectUrl { get; set; }

        public string IconUrl { get; set; }

        public string LicenseUrl { get; set; }

        public string Copyright { get; set; }

        public bool RequireLicenseAcceptance { get; set; }

        public ICollection<Dependency> Dependencies { get; private set; }

        public ICollection<string> Tags { get; private set; }

        public ICollection<FrameworkAssembly> FrameworkAssemblies { get; private set; }

        public ICollection<File> Files { get; private set; }
    }
}