﻿namespace EvilDev.NugetAutoPackager.Model
{
    public class File
    {
        public string Src { get; set; }

        public string Target { get; set; }

        public string Exclude { get; set; }
    }
}