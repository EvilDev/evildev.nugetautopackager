﻿namespace EvilDev.NugetAutoPackager.Model
{
    public class FrameworkAssembly
    {
        public string AssemblyName { get; set; }
    }
}