﻿using System.Collections.Generic;
using System.IO;
using System.Xml;
using System.Xml.XPath;
using EvilDev.NugetAutoPackager.Constants;
using EvilDev.NugetAutoPackager.Model;

namespace EvilDev.NugetAutoPackager.Parsers
{
    public class ProjectFileParser : IProjectFileParser
    {
        // Project/ItemGroup/ProjectReference/Name

        private const string XpathQuery = "build:Project/build:ItemGroup/build:ProjectReference/build:Name";
        private readonly ICollection<Dependency> _dependencies;
        private readonly string _projectFilePath;

        public ProjectFileParser(ISettingsProvider settings)
        {
            _dependencies = new List<Dependency>();
            _projectFilePath = Path.GetFullPath(settings[CommandLineArgs.ProjectFilePath]);
        }

        public IEnumerable<Dependency> ListDependencies()
        {
            var xml = new XPathDocument(_projectFilePath);
            var navigator = xml.CreateNavigator();
            var ns = new XmlNamespaceManager(navigator.NameTable);
            ns.AddNamespace("build", "http://schemas.microsoft.com/developer/msbuild/2003");
            var nodes = navigator.Select(XpathQuery, ns);

            while (nodes.MoveNext())
            {
                var dep = new Dependency
                    {
                        Id = nodes.Current.InnerXml
                    };

                _dependencies.Add(dep);
            }

            return _dependencies;
        }
    }
}