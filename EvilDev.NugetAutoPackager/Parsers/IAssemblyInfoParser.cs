﻿using System;
using System.Linq.Expressions;

namespace EvilDev.NugetAutoPackager.Parsers
{
    public interface IAssemblyInfoParser
    {
        string Parse(Expression<Func<AssemblyInformation, string>> field);
    }
}