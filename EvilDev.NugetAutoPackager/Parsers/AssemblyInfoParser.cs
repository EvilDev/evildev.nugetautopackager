﻿using System;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Text.RegularExpressions;
using EvilDev.NugetAutoPackager.Constants;

namespace EvilDev.NugetAutoPackager.Parsers
{
    public class AssemblyInfoParser : IAssemblyInfoParser
    {
        private readonly ISettingsProvider _settings;
        private readonly string _fileContents;
        private const string LineRegex = @"\[assembly: Assembly{0}\(\"".+\""\)\]";
        private const string ValueRegex = @"\(.+\)";

        private static string GetPropertyNameFromExpression<TField>(Expression<Func<AssemblyInformation, TField>> field)
        {
            return (field.Body as MemberExpression).Member.Name;
        }

        public AssemblyInfoParser(ISettingsProvider settings)
        {
            _settings = settings;
            var projectDir = new FileInfo(Path.GetFullPath(_settings[CommandLineArgs.ProjectFilePath])).Directory;
            var assemblyInfoFile = projectDir.GetFiles("AssemblyInfo.cs", SearchOption.AllDirectories).First();

            using (var reader = new StreamReader(assemblyInfoFile.FullName))
            {
                _fileContents = reader.ReadToEnd();
            }
        }

        public string Parse(Expression<Func<AssemblyInformation, string>> field)
        {
            var property = GetPropertyNameFromExpression(field);
            var match = new Regex(string.Format(LineRegex, property)).Match(_fileContents);
            return new Regex(ValueRegex).Match(match.Value).Value.TrimStart('(').TrimEnd(')').Replace("\"", string.Empty);
        }
    }
}