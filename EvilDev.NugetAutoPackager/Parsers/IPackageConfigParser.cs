﻿using System.Collections.Generic;
using EvilDev.NugetAutoPackager.Model;

namespace EvilDev.NugetAutoPackager.Parsers
{
    public interface IPackageConfigParser
    {
        IEnumerable<Dependency> ListDependencies();
    }
}