﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml.XPath;
using Castle.Core.Internal;
using EvilDev.NugetAutoPackager.Constants;
using EvilDev.NugetAutoPackager.Model;

namespace EvilDev.NugetAutoPackager.Parsers
{
    public class PackageConfigParser : IPackageConfigParser
    {
        private readonly ICollection<Dependency> _dependencies;
        private readonly DirectoryInfo _projectDir;

        private const string XpathQuery = "packages/package";

        public PackageConfigParser(ISettingsProvider settings)
        {
            _dependencies = new List<Dependency>();
            _projectDir = new FileInfo(Path.GetFullPath(settings[CommandLineArgs.ProjectFilePath])).Directory;
        }

        public IEnumerable<Dependency> ListDependencies()
        {
            _projectDir.GetFiles("Packages.config", SearchOption.AllDirectories)
                       .AsEnumerable()
                       .ForEach(file =>
                           {
                               var xml = new XPathDocument(file.FullName);
                               var navigator = xml.CreateNavigator();
                               var nodes = navigator.Select(XpathQuery);
                               while (nodes.MoveNext())
                               {
                                   var dep = new Dependency
                                       {
                                           Id = nodes.Current.GetAttribute("id", string.Empty),
                                           Version = nodes.Current.GetAttribute("version", string.Empty)
                                       };

                                   _dependencies.Add(dep);
                               }
                           });

            return _dependencies;
        }
    }
}