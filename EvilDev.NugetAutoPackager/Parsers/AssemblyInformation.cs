﻿using System.Collections.Generic;

namespace EvilDev.NugetAutoPackager.Parsers
{
    public class AssemblyInformation
    {
        public string Title { get; set; }

        public string Description { get; set; }

        public string Configuration { get; set; }

        public string Company { get; set; }

        public string Product { get; set; }

        public string Copyright { get; set; }

        public string Trademark { get; set; }

        public string Culture { get; set; }

        public string Summary { get; set; }

        public string ProjectUrl { get; set; }

        public string IconUrl { get; set; }

        public string Version { get; set; }

        public string LicenseUrl { get; set; }

        public string Tags { get; set; }
    }
}