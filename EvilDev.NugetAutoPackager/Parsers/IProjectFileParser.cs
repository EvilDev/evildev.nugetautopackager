﻿using System.Collections.Generic;
using EvilDev.NugetAutoPackager.Model;

namespace EvilDev.NugetAutoPackager.Parsers
{
    public interface IProjectFileParser
    {
        IEnumerable<Dependency> ListDependencies();
    }
}