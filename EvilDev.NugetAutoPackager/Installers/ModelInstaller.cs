﻿using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;
using EvilDev.NugetAutoPackager.Model;

namespace EvilDev.NugetAutoPackager.Installers
{
    public class ModelInstaller : IWindsorInstaller
    {
        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            container.Register(Classes.FromThisAssembly()
                                      .InSameNamespaceAs<Nuspec>()
                                      .Unless(t => t == typeof (Nuspec))
                                      .WithServiceSelf()
                                      .LifestyleTransient());

            container.Register(Component.For<Nuspec>().ImplementedBy<Nuspec>());
        }
    }
}