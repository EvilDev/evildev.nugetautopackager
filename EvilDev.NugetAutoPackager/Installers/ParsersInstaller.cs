﻿using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;

namespace EvilDev.NugetAutoPackager.Installers
{
    public class ParsersInstaller : IWindsorInstaller
    {
        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            container.Register(Classes.FromThisAssembly()
                                      .Where(t => t.Name.EndsWith("Parser"))
                                      .WithServiceDefaultInterfaces());
        }
    }
}