﻿namespace EvilDev.NugetAutoPackager
{
    public enum ExitCodes : int
    {
        FileSystemError = 1,
        UnknownError = 10
    }
}