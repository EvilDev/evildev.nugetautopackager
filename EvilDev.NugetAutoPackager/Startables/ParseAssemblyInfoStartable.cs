﻿using System.Linq;
using Castle.Core;
using Castle.Core.Internal;
using EvilDev.NugetAutoPackager.Model;
using EvilDev.NugetAutoPackager.Parsers;

namespace EvilDev.NugetAutoPackager.Startables
{
    public class ParseAssemblyInfoStartable : IStartable
    {
        private readonly IAssemblyInfoParser _parser;
        private readonly Nuspec _spec;

        public ParseAssemblyInfoStartable(IAssemblyInfoParser parser, Nuspec spec)
        {
            _parser = parser;
            _spec = spec;
        }

        public void Start()
        {
            _spec.Id = _parser.Parse(a => a.Title);
            _spec.Language = _parser.Parse(a => a.Culture);
            _spec.Description = _parser.Parse(a => a.Description);
            _spec.Summary = _parser.Parse(a => a.Summary);
            _spec.ProjectUrl = _parser.Parse(a => a.ProjectUrl);
            _spec.IconUrl = _parser.Parse(a => a.IconUrl);
            _spec.Owners.Add(_parser.Parse(a => a.Company));
            _spec.Copyright = _parser.Parse(a => a.Copyright);
            _spec.Version = _parser.Parse(a => a.Version);
            _spec.Authors.Add(_parser.Parse(a => a.Copyright));
            _spec.LicenseUrl = _parser.Parse(a => a.LicenseUrl);
            _spec.RequireLicenseAcceptance = !string.IsNullOrEmpty(_spec.LicenseUrl);
            _parser.Parse(a => a.Tags).Split(',').AsEnumerable()
                   .ForEach(_spec.Tags.Add);
        }

        public void Stop()
        {
        }
    }
}