﻿using Castle.Core;
using Castle.Core.Internal;
using EvilDev.NugetAutoPackager.Model;
using EvilDev.NugetAutoPackager.Parsers;

namespace EvilDev.NugetAutoPackager.Startables
{
    public class ParseProjectFileStartable : IStartable
    {
        private readonly IProjectFileParser _parser;
        private readonly Nuspec _spec;
        private readonly ParseAssemblyInfoStartable _depTask;

        public ParseProjectFileStartable(IProjectFileParser parser, Nuspec spec, ParseAssemblyInfoStartable depTask)
        {
            _parser = parser;
            _spec = spec;
            _depTask = depTask;
        }

        public void Start()
        {
            _parser.ListDependencies()
                   .ForEach(dep =>
                       {
                           dep.Version = _spec.Version;
                           _spec.Dependencies.Add(dep);
                       });
        }

        public void Stop()
        {
        }
    }
}