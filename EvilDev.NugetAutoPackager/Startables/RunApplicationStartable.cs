﻿using System.IO;
using System.Linq;
using System.Xml;
using Castle.Core;
using EvilDev.NugetAutoPackager.Constants;
using EvilDev.NugetAutoPackager.Model;

namespace EvilDev.NugetAutoPackager.Startables
{
    public class RunApplicationStartable : IStartable
    {
        private readonly ISettingsProvider _settings;
        private readonly Nuspec _spec;

        public RunApplicationStartable(ParseAssemblyInfoStartable dep1,
                                       ParsePackageConfigFilesStartable dep2,
                                       ParseProjectFileStartable dep3,
                                       ISettingsProvider settings,
                                       Nuspec spec)
        {
            _settings = settings;
            _spec = spec;
        }

        private static string CreateIfDoesntExist(string dirpath)
        {
            if (!Directory.Exists(dirpath)) Directory.CreateDirectory(dirpath);

            return dirpath;
        }

        public void Start()
        {
            var projectFilePath = Path.GetFullPath(_settings[CommandLineArgs.ProjectFilePath]);
            var projectDir = Path.GetDirectoryName(projectFilePath) ?? string.Empty;
            var packageDir = CreateIfDoesntExist(Path.Combine(projectDir, "Publish"));

            var libDir = CreateIfDoesntExist(Path.Combine(packageDir, "lib"));

            var assembly = new DirectoryInfo(projectDir)
                .GetFiles(string.Format("{0}.dll", _spec.Id), SearchOption.AllDirectories)
                .Where(file => !file.DirectoryName.StartsWith(packageDir))
                .OrderByDescending(file => file.CreationTime)
                .FirstOrDefault();

            if (assembly != null)
            {
                var destination = Path.Combine(libDir, assembly.Name);
                if (System.IO.File.Exists(destination)) System.IO.File.Delete(destination);

                System.IO.File.Copy(assembly.FullName, destination, true);
            }

            using (var sw = new StreamWriter(Path.Combine(packageDir, string.Format("{0}.nuspec", _spec.Id))))
            {
                using (var xml = new XmlTextWriter(sw))
                {
                    xml.Formatting = Formatting.Indented;
                    xml.WriteStartDocument();
                    xml.WriteStartElement("package");

                    xml.WriteStartElement("metadata");
                    
                    xml.WriteElementString("id", _spec.Id);
                    xml.WriteElementString("version", _spec.Version);
                    xml.WriteElementString("authors", string.Join(", ", _spec.Authors));
                    xml.WriteElementString("owners", string.Join(", ", _spec.Owners));
                    xml.WriteElementString("projectUrl", _spec.ProjectUrl);
                    xml.WriteElementString("iconUrl", _spec.IconUrl);
                    xml.WriteElementString("requireLicenseAcceptance", _spec.RequireLicenseAcceptance.ToString().ToLower());                    xml.WriteElementString("licenseUrl", _spec.LicenseUrl);
                    xml.WriteElementString("description", _spec.Description);
                    xml.WriteElementString("copyright", _spec.Copyright);
                    xml.WriteElementString("releaseNotes", _spec.ReleaseNotes);
                    xml.WriteElementString("tags", string.Join("", _spec.Tags).TrimEnd(' '));
                    xml.WriteElementString("summary", _spec.Summary);
                    xml.WriteElementString("language", _spec.Language);

                    xml.WriteStartElement("dependencies");

                    foreach (var dep in _spec.Dependencies)
                    {
                        xml.WriteStartElement("dependency");
                        xml.WriteAttributeString("id", dep.Id);
                        xml.WriteAttributeString("version", dep.Version);
                        xml.WriteEndElement();
                    }

                    xml.WriteEndElement();

                    xml.WriteEndElement();

                    xml.WriteStartElement("files");

                    xml.WriteStartElement("file");
                    xml.WriteAttributeString("src", string.Format("{0}\\**", libDir));
                    xml.WriteAttributeString("target", "lib");
                    xml.WriteEndElement();

                    xml.WriteEndElement();

                    xml.WriteEndElement();
                    xml.WriteEndDocument();
                }
            }
        }

        public void Stop()
        {
        }
    }
}