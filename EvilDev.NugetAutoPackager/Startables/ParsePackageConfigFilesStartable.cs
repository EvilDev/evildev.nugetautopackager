﻿using Castle.Core;
using Castle.Core.Internal;
using EvilDev.NugetAutoPackager.Model;
using EvilDev.NugetAutoPackager.Parsers;

namespace EvilDev.NugetAutoPackager.Startables
{
    public class ParsePackageConfigFilesStartable : IStartable
    {
        private readonly IPackageConfigParser _parser;
        private readonly Nuspec _spec;

        public ParsePackageConfigFilesStartable(IPackageConfigParser parser, Nuspec spec)
        {
            _parser = parser;
            _spec = spec;
        }

        public void Start()
        {
            _parser.ListDependencies()
                   .ForEach(dep => _spec.Dependencies.Add(dep));
        }

        public void Stop()
        {
        }
    }
}