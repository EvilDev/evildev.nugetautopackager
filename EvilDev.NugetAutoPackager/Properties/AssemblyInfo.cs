﻿using System.Reflection;
using EvilDev.Commons.Attributes;

[assembly: AssemblyTitle("EvilDev.NugetAutoPackager")]
[assembly: AssemblySummary("Generates nuspec files for a project by convention")]
[assembly: AssemblyDescription("Generates nuspec files for a project by convention")]
[assembly: AssemblyProjectUrl("https://bitbucket.org/EvilDev/evildev.nugetautopackager")]
[assembly: AssemblyIconUrl("https://bitbucket-assetroot.s3.amazonaws.com/c/photos/2012/Dec/06/evildev.commons-logo-3272278768-2_avatar.png")]
[assembly: AssemblyCompany("Deville Solutions")]
[assembly: AssemblyProduct("EvilDev.NugetAutoPackager")]
[assembly: AssemblyCopyright("Copyright © Deville Solutions 2012")]
[assembly: AssemblyVersion("1.0.0.0")]
[assembly: AssemblyTags("Nuget", "Nuspec")]