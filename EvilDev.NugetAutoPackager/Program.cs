﻿using System;
using System.IO;
using Castle.Facilities.Startable;
using Castle.MicroKernel.Registration;
using Castle.Windsor;
using Castle.Windsor.Installer;

namespace EvilDev.NugetAutoPackager
{
    public class Program
    {
        static void Main(string[] args)
        {
            try
            {
                var container = new WindsorContainer();
                container.AddFacility<StartableFacility>();

                container.Install(FromAssembly.This());

                container.Register(Component.For<ISettingsProvider>()
                                            .Instance(new CommandLineSettingsProvider(args)));

                container.Dispose();
            }
            catch (IOException ex)
            {
                WriteError(ex);
                Environment.Exit((int) ExitCodes.FileSystemError);
            }
            catch (Exception ex)
            {
                WriteError(ex);
                Environment.Exit((int) ExitCodes.UnknownError);
            }
        }

        private static void WriteError(Exception ex)
        {
            Console.Error.Write("Error: {1}{0}Source:{2}{0}Stack Trace{3}", Environment.NewLine, ex.Message, ex.Source, ex.StackTrace);
        }
    }
}
