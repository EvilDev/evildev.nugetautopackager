﻿using System.Collections.Generic;
using System.Linq;

namespace EvilDev.NugetAutoPackager
{
    public class CommandLineSettingsProvider : ISettingsProvider
    {
        private readonly Dictionary<string, string> _settings;

        public CommandLineSettingsProvider(IEnumerable<string> args)
        {
            _settings = args.ToDictionary(str => str.Split('=')[0], str => str.Split('=')[1]);
        }

        public string this[string key]
        {
            get { return _settings[key]; }
            set { _settings[key] = value; }
        }

        public bool ContainsKey(string key)
        {
            return _settings.ContainsKey(key);
        }
    }
}